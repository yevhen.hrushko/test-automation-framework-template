package gatling.test.example.simulation;

import static gatling.test.example.simulation.SystemPropertiesUtil.getAsDoubleOrElse;
import static gatling.test.example.simulation.SystemPropertiesUtil.getAsIntOrElse;
import static gatling.test.example.simulation.SystemPropertiesUtil.getAsStringOrElse;

public final class PerfTestConfig {

    private PerfTestConfig() {}
    static final String BASE_URL = getAsStringOrElse("baseUrl", "https://api.instagram.com/");
    static final double REQUEST_PER_SECOND = getAsDoubleOrElse("requestPerSecond", 1f);
    static final long DURATION_MIN = getAsIntOrElse("durationMin", 1);
    static final int P95_RESPONSE_TIME_MS = getAsIntOrElse("p95ResponseTimeMs", 1000);
}
