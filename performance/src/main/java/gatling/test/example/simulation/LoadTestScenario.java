package gatling.test.example.simulation;

import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.http;

// https://gatling.io/docs/gatling/tutorials/quickstart/
public class LoadTestScenario extends Simulation {

    String baseUrl = "https://api.instagram.com";
    String environment = "UAT";
    String barerToken ="";
    String fetchEndpoint = "/v1/service/test";
    String body = "{}";

    // 4. The common configuration to all HTTP requests.
    HttpProtocolBuilder httpProtocol = http
            // 5. The baseUrl that will be prepended to all relative urls.
            .baseUrl("https://api.instagram.com/")
             // 6. Common HTTP headers that will be sent with all the requests.
            .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
            .doNotTrackHeader("1")
            .acceptLanguageHeader("en-US,en;q=0.5")
            .acceptEncodingHeader("gzip, deflate")
            //.userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
            .userAgentHeader("Performance Tool Yevhen Hrushko")
            .header("Authorization", barerToken)
            .header("Content-Type", "application/json");

    // 7 The scenario definition.
    ScenarioBuilder scn = scenario("BasicSimulation")
            // 8 An HTTP request, named "request_1". This name will be displayed in the final reports.
            .exec(http(baseUrl + environment)
                    // 9 The url this request targets with the GET method.
                    .post(fetchEndpoint)
                    .body(StringBody(body)).asJson()
            )
            // 10 Some pause/think time.
            .pause(3);

    /*
    ScenarioBuilder scn2 = scenario("BasicSimulation")
            // 8 An HTTP request, named "request_1". This name will be displayed in the final reports.
            .exec(http(baseUrl + environment)
                    // 9 The url this request targets with the GET method.
                    .post(fetchEndpoint)
                    .body(StringBody(body)).asJson()
            )
            // 10 Some pause/think time.
            .pause(3);
    */
    public LoadTestScenario() {
        // 11 Where one sets up the scenarios that will be launched in this Simulation.
        setUp(
                /*  Duration units default to seconds, e.g. pause(5) is equivalent to java.time.Duration.ofSeconds(5) in Java or pause(5.seconds) in Scala. */
                // 12 Declaring that we will inject one single user into the scenario named scn.
                scn.injectOpen(atOnceUsers(1))
                // 13 Attaching the HTTP configuration declared above.
        ).protocols(httpProtocol);
    }


    /* {
        setUp(scn2.injectOpen(
        // constantUsersPerSec(REQUEST_PER_SECOND
        //rampUsers(120).during(Duration.ofMinutes(DURATION_MIN))
        constantUsersPerSec(10).during(Duration.ofMinutes(DURATION_MIN))
        ))
        .protocols(httpProtocol)
        .assertions(global().responseTime().percentile3().lt(P95_RESPONSE_TIME_MS),
         global().successfulRequests().percent().gt(95.0));
    } */

}
